<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FtpController extends Controller
{
    public function index()
    {
        return Storage::disk('ftp')->download('test/RECON-17-06-2020-17-02-47.pdf');
    }

    public function coba()
    {
        return view('ftp/coba');
    }

    public function download(Request $request)
    {
        return  \Storage::disk('ftp')->download($request->path) ;
    }
}
